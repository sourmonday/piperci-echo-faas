# PiperCI Echo Function

### Table of Contents

* [Getting Started](#getting-started)
* [Prerequisites](#prerequisites)
* [Installing](#installing)
* [Inputs and Outputs](#inputs-and-outputs)
* [Running the Tests](#running-the-tests)
* [Contributing](#contributing)
* [Versioning](#versioning)
* [License](#license)
* [Acknowledgements](#acknowledgments)


## Getting Started

To deploy this function you must have OpenFaaS installed. To create a development environment see 
(https://gitlab.com/dreamer-labs/piperci/piperci-installer)

### Prerequisites

OpenFaaS

### Installing

To install this function on OpenFaaS do the following after authentication:

```
git clone https://gitlab.com/dreamer-labs/piperci/piperci-echo-faas.git
cd piperci-echo-faas
faas-cli template pull https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates
faas build
faas deploy
```

To validate that your function installed correctly you can run the following:

```
faas ls
```

## Inputs and Outputs


## Running the tests

```bash
tox -e lint
```

### Test Prerequisites

Tox must be installed and an OpenFaaS environment must be available locally.
You must also deploy the image to OpenFaaS.

There is an simple bash script which can be used to turn a local machine into
an OpenFaaS development environment. This can be found in `tools/scripts/setup-env.sh`.
This is the script that is being used by Travis-CI to deploy the test environment.

We also have an ansible role available to setup the OpenFaaS environment. This
can be found [here](https://gitlab.com/dreamer-labs/piperci/piperci-installer)

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/piperci/piperci-boilerplates/blob/master/CONTRIBUTING.md) for details 
on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the 
[tags on this repository](https://gitlab.com/piperci-echo-faas/tags).

## License

MIT
