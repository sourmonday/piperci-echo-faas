import pytest
import responses

from piperci.faas.exceptions import PiperDelegateError
from piperci.storeman.client import storage_client
from typing import Tuple, Union


def test_gateway_no_json(client):
    headers = {"Content-Type": "application/json"}
    resp = client.post("test", headers=headers)
    assert resp.status_code == 422


def test_gateway_no_content_type_json(client, single_instance):
    resp = client.post("test", data=single_instance)
    assert resp.status_code == 422


@responses.activate
@pytest.mark.parametrize(
    "valid_command_replacement",
    [
        'echo "hello world"',
        ['echo "echo in a list"'],
        ['echo "multiple echos 1"', 'echo "multiple echos 2"'],
    ],
)
def test_gateway(
    client,
    common_headers,
    minio_server,
    mocker,
    single_instance,
    start_task_response,
    task_artifact_upload_response,
    task_creation_response,
    task_post_executor_url_response,
    valid_command_replacement,
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(*task_post_executor_url_response[0])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

    mocker.patch("piperci.faas.this_task.ThisTask._init_storage_client", store_cli)

    single_instance["run-cmd"] = valid_command_replacement
    resp = client.post("test", json=single_instance, headers=common_headers)
    assert resp.status_code == 200


@responses.activate
@pytest.mark.parametrize("missing_field_name", ["run-cmd"])
def test_rejects_required_fields_missing_requests(
    client,
    common_headers,
    missing_field_name,
    mocker,
    start_task_response,
    task_creation_response,
    valid_echo_request: Tuple[dict, str],
):
    request_json, _ = valid_echo_request

    del request_json[missing_field_name]
    assert missing_field_name not in request_json.keys()

    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])

    mock_delegate = mocker.patch("piperci.faas.this_task.ThisTask.delegate")
    mocker.patch("piperci.faas.this_task.ThisTask.complete", return_value=(None, 200))

    resp = client.get("test", json=request_json, headers=common_headers)
    assert resp.status_code == 400
    mock_delegate.assert_not_called()


@pytest.mark.parametrize(
    "invalid_command_replacement",
    [
        "fake-cmd arg2",
        "pwd",
        ["pwd", "fake-cmd arg2"],
        ["pwd"],
        ["echo 'this is valid but'", "this-is-not"],
        ["this-is-not-valid-but", "echo 'this is valid'"],
        {"key": "this is not a valid type"},
    ],
)
@responses.activate
def test_validate_command_installed_invalid_inputs(
    client,
    common_headers,
    mocker,
    start_task_response,
    task_creation_response,
    invalid_command_request: Tuple[dict, str],
    invalid_command_replacement: Union[list, str],
):
    request_json, _ = invalid_command_request
    request_json["run-cmd"] = invalid_command_replacement

    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])

    mock_delegate = mocker.patch("piperci.faas.this_task.ThisTask.delegate")
    mocker.patch("piperci.faas.this_task.ThisTask.complete", return_value=(None, 200))

    resp = client.get("test", json=request_json, headers=common_headers)
    assert resp.status_code == 400
    mock_delegate.assert_not_called()


@responses.activate
def test_gateway_delegation_fail(
    client,
    common_headers,
    minio_server,
    mocker,
    start_task_response,
    task_artifact_upload_response,
    task_creation_response,
    task_post_executor_url_response,
    valid_echo_request,
):
    request_json, _ = valid_echo_request

    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(*task_post_executor_url_response[0])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

    mocker.patch("piperci.faas.this_task.ThisTask._init_storage_client", store_cli)
    mocker.patch(
        "piperci.faas.this_task.ThisTask.delegate", side_effect=PiperDelegateError
    )

    resp = client.get("test", json=request_json, headers=common_headers)
    assert resp.status_code == 400


@responses.activate
def test_gateway_client_task_info_fail(
    client, common_headers, mocker, single_instance, start_task_response
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    mock_delegate = mocker.patch("piperci.faas.this_task.ThisTask.delegate")

    resp = client.post("test", json=single_instance, headers=common_headers)
    assert resp.status_code == 400
    mock_delegate.assert_not_called()
