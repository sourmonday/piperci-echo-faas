import subprocess

from flask import Request
from shutil import which
from pathlib import Path
from typing import Dict, Tuple, Union

from piperci.faas.exceptions import PiperError
from piperci.faas.this_task import ThisTask


class ValidationError(Exception):
    pass


def handle(
    request: Request, task: ThisTask, config: Dict
) -> Tuple[Union[Dict, str], int]:
    """
    Entrypoint to the echo faas async executor function.
    :param request: The request object from Flask
    :param task: The ThisTask instance for this class required for this faas
    :param config:
    :return: (response_body, code)
    """
    try:
        task.info(f"{str(task.task)} echo faas executor called successfully")
    except PiperError as e:
        return {"error": f"{str(e)}: task communication with gman failed"}, 400

    try:
        task.info(f"request json body: {request.get_json()}")
        validate(request=request, config=config)
    except ValidationError as e:
        task.fail(
            f"Task {task.task['task']['task_id']} has failed due to validation error"
        )
        return {"error": f"{str(e)}: validation failed"}, 400
    except TypeError as e:
        return {"error": f"{str(e)}: validation failed due to type mismatch"}, 400

    try:
        artfile_path = f'test_artifact_{task.task["task"]["task_id"]}'
        execute(request, task, Path(artfile_path))

        task.artifact(artfile_path)
        task.complete("Echo faas complete")
        return {"test": "test"}, 200
    except PiperError:
        task.fail(
            f"Task {task.task['task']['task_id']} has failed due to unforeseen error"
        )
        return "Unforeseen error", 400


def execute(request: Request, task: ThisTask, write_to: Union[Path, str]):
    try:
        task.info("Executing request for command passed")
        request_command_param = request.get_json()["run-cmd"]

        if isinstance(request_command_param, str):
            request_command = request_command_param.split()
            output = subprocess.check_output(request_command, shell=True)
            artifact = f'{task.task["task"]["task_id"]}: {output}'
        else:
            artifact = ""
            request_commands = list(map(lambda x: x.split(), request_command_param))

            for request_command in request_commands:
                output = subprocess.check_output(request_command, shell=True)
                artifact += f'{task.task["task"]["task_id"]}: {output}\n\n\n'

        with open(write_to, "w") as artfile:
            artfile.write(artifact)
    except subprocess.CalledProcessError:
        task.info("Error calling passed command in executor")
        raise PiperError


def validate(request: Request, config: dict, **kwargs):
    if "task" in kwargs:
        task = kwargs["task"]
    else:
        task = None

    try:
        validate_required_field_in_request(request=request, task=task)
        validate_commands_installed(request=request, config=config, task=task)
    except AssertionError:
        raise ValidationError


def validate_required_field_in_request(request: Request, task: ThisTask = None):
    request_json = request.get_json()
    required_json_fields = {"run-cmd"}

    for field in required_json_fields:
        assert field in request_json.keys()

    if task:
        task.info(
            f"request json passed required fields validation for fields: "
            + f"{required_json_fields}"
        )


def validate_commands_installed(request: Request, config: dict, task: ThisTask = None):
    request_json = request.get_json()
    request_command_param = request_json["run-cmd"]
    supported_commands = config["supported_commands"]

    if isinstance(request_command_param, str):
        request_program = request_command_param.split()[0]
        assert which(request_program) is not None
        assert request_program in supported_commands
    elif isinstance(request_command_param, list):
        request_programs = list(map(lambda x: x.split()[0], request_command_param))
        for request_program in request_programs:
            assert which(request_program) is not None
            assert request_program in supported_commands
    else:
        if task:
            task.info(f"run-cmd in request json did not match expected type and "
                      + f"instead is type {type(request_command_param)}")
        raise TypeError
